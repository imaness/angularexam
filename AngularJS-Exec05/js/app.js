angular.module('app', ['ngRoute']);

(function () {
    var myApp = angular.module('app', ['ngRoute']);


    var listController = function ($http,CustomerService) {
        var self = this;
        self.customers = [];

        CustomerService.getCustomer().success(function (response) {
            self.customers = response.customers;
        });
        
    };

    var detailController = function ($http, $routeParams, CustomerService) {
        var self = this;
        self.customers = [];

        CustomerService.getCustomer().success(function (response) {
            self.customers = response.customers;
            self.customer = self.customers[$routeParams.id - 1];
        });

    };

    var CustomerService = function ($http) {
        var customers = [];
        var getCustomer = function () {
            return $http.get('customers.json').success(function (response) {
                customers = reponse.customers;
            });

        };

        return {
            getCustomer: getCustomer,
            customers: customers
        };

    };

    

    // Inside this controller, call the service created before to show the list contents in partials/list.html
    myApp.controller('ListController', ['$http','CustomerService', listController]);
    // Call the service created before to show customer details, according to the route, in partials/detail.html
    myApp.controller('DetailController', ['$http', '$routeParams','CustomerService', detailController]);
    // Create a service to get the JSON file result.
    myApp.factory('CustomerService', ['$http',CustomerService]);
    // Create two routes - one for the root (list) and another for customer details.
    // Provide a fallback to redirect to the root whenever the requested doesn't exist.
    myApp.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'ListController',
                templateUrl: 'partials/list.html'
            })
            .when('/Detail/:id', {
                controller: 'DetailController',
                templateUrl: 'partials/detail.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });
}());
