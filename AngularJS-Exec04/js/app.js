/// <reference path="../../Scripts/angular.intellisense.js" />
myApp = angular.module('app', ['ngRoute', 'ngSanitize', 'ui.select']);

(function () {
    var todo = function ($scope, $location, $routeParams, $filter, ToDoService) {
        var self = this;
        self.tagList = [];
        self.test = 0;
        self.id = $routeParams.id;
        
        self.toDos = ToDoService.getToDoList();
        
        var onlyUnique = function (value, index, self) {
            return self.indexOf(value) === index;
        };

        self.AddToDo = function () {
            self.toDo.id = self.toDos.length;
            ToDoService.addToDo(self.toDo);
            //self.toDos.push(self.toDo);
            self.toDo = {};
            $location.path("/");
        };
        
        self.removeToDo = function (item) {
            ToDoService.removeToDo(item);
            self.toDos = _.without(self.toDos, _.findWhere(self.toDos, { id: item.id }));
        };
    };

    var editTodo = function ($scope, $location, $routeParams, $filter, ToDoService) {
        var self = this;
        self.toDos = ToDoService.getToDoList();
        self.toDo = $filter('filter')(self.toDos, { id: $routeParams.id })[0];

        self.editToDo = function (id) {
            ToDoService.editToDo(self.toDo);
            $location.path("/");
        };

    };

    myApp.config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'ToDoController',
                templateUrl: 'Home.html'
            })
            .when('/Edit/:id', {
                controller: 'EditToDoController',
                templateUrl: 'Edit.html'
            })
            .when('/Add', {
                controller: 'ToDoController',
                templateUrl: 'Add.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    });

    myApp.controller('ToDoController', ['$scope', '$location', '$routeParams', '$filter', 'ToDoService', todo]);
    myApp.controller('EditToDoController', ['$scope', '$location', '$routeParams', '$filter', 'ToDoService', editTodo]);
    
}());

