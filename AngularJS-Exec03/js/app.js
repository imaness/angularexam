/// <reference path="../../Scripts/angular.js" />


(function () {
    var myApp = angular.module('app', ['ordinal'])


    myApp.controller('AppController', ['$http', function ($http) {
        var self = this;
        self.F1result = {};

        $http.get("http://ergast.com/api/f1/current/last/results.json").success(function (response) {
            self.F1result = response.MRData.RaceTable.Races;
        });

    }]);
}());
