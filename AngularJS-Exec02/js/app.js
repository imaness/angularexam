/// <reference path="../../Scripts/angular.js" />


(function () {
   var myApp =  angular.module('app', [])
                  

   myApp.controller('AppController',['$http', function ($http) {
       var self = this;
       self.customers = {};
       $http.get('customers.json').success(function(response) {
           self.customers = response.customers;
       });
   }]);
}());
